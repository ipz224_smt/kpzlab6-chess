﻿using System;
using System.Drawing;
using System.Windows.Forms;
using ChessBoard;

namespace Chess
{
    public static class BoardRenderer
    {
        public static void DrawPieces(Board chessBoard, Button[,] btnGrid)
        {
            chessBoard.GoThroughAllBoard((x, y) =>
            {
                if (chessBoard.Grid[x, y].Piece == null)
                {
                    btnGrid[x, y].Image = null;
                    return true;
                }

                if (chessBoard.Grid[x, y].Team == "White")
                {
                    btnGrid[x, y].Image = GetImage(chessBoard.Grid[x, y].Piece, "White");
                }
                else if (chessBoard.Grid[x, y].Team == "Black")
                {
                    btnGrid[x, y].Image = GetImage(chessBoard.Grid[x, y].Piece, "Black");
                }

                btnGrid[x, y].ImageAlign = ContentAlignment.MiddleCenter;

                if (chessBoard.Grid[x, y].IsCheckPath)
                {
                    btnGrid[x, y].BackColor = ((x % 2 == 0 && y % 2 == 0) || (x % 2 != 0 && y % 2 != 0))
                        ? Color.LightSalmon
                        : Color.Salmon;
                }

                return true;
            });
        }

        private static Image GetImage(string piece, string team)
        {
            string pieceName = team + piece;
            switch (pieceName)
            {
                case "WhitePawn":
                    return Image.FromFile(@"..\..\image\WhitePawn.png");
                case "WhiteRook":
                    return Image.FromFile(@"..\..\image\WhiteRook.png");
                case "WhiteHorse":
                    return Image.FromFile(@"..\..\image\WhiteHorse.png");
                case "WhiteBishop":
                    return Image.FromFile(@"..\..\image\WhiteBishop.png");
                case "WhiteQueen":
                    return Image.FromFile(@"..\..\image\WhiteQueen.png");
                case "WhiteKing":
                    return Image.FromFile(@"..\..\image\WhiteKing.png");
                case "BlackPawn":
                    return Image.FromFile(@"..\..\image\BlackPawn.png");
                case "BlackRook":
                    return Image.FromFile(@"..\..\image\BlackRook.png");
                case "BlackHorse":
                    return Image.FromFile(@"..\..\image\BlackHorse.png");
                case "BlackBishop":
                    return Image.FromFile(@"..\..\image\BlackBishop.png");
                case "BlackQueen":
                    return Image.FromFile(@"..\..\image\BlackQueen.png");
                case "BlackKing":
                    return Image.FromFile(@"..\..\image\BlackKing.png");
                default:
                    throw new ArgumentException("Unknown piece: " + piece);
            }
        }
    }
}
