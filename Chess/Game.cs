﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using ChessBoard;

namespace Chess
{
    public partial class Game : Form
    {
        private static Board chessBoard = new Board(8);
        public Button[,] btnGrid = new Button[chessBoard.Size, chessBoard.Size];

        private string chessPiece;
        private string lastPiece;
        private string lastPieceTeam;
        private int moveCounter = 0;
        private int lastX;
        private int lastY;
        private List<string> capturedWhite = new List<string>();
        private List<string> capturedBlack = new List<string>();
        private bool isMoving = false;
        private bool iswhiteMoving = false;

        public Game()
        {
            InitializeComponent();
            populateGrid();
            DrawPieces();
            SetBoardColor();
        }

        // Відповідає за створення кнопок та додавання їх на форму
        private void PopulateButtons()
        {
            int buttonSize = panel1.Width / chessBoard.Size;
            panel1.Height = panel1.Width;

            chessBoard.GoThroughAllBoard((x, y) =>
            {
                InitializeButton(x, y, buttonSize);

                return true;
            });
        }

        // Метод ініціалізує одну кнопку шахової дошки зазначеними розмірами та позицією
        private void InitializeButton(int x, int y, int buttonSize)
        {
            btnGrid[x, y] = new Button()
            {
                Height = buttonSize,
                Width = buttonSize,
                FlatStyle = FlatStyle.Flat,
                Location = new Point(x * buttonSize, y * buttonSize),
                Tag = new ButtonTag()
                {
                    ButtonPosition = new Point(x, y)
                }
            };
            btnGrid[x, y].FlatAppearance.BorderSize = 0;
            btnGrid[x, y].Click += Grid_Button_Click;
            panel1.Controls.Add(btnGrid[x, y]);
        }

        // Метод встановлює початкові позиції шахових фігур на дошці
        private void SetInitialPiecePositions()
        {
            string[] pieces = { "Rook", "Horse", "Bishop", "Queen", "King", "Bishop", "Horse", "Rook" };

            for (int i = 0; i < 8; i++)
            {
                chessBoard.Grid[i, 6].Piece = "Pawn";
                chessBoard.Grid[i, 6].Team = "White";
                chessBoard.Grid[i, 1].Piece = "Pawn";
                chessBoard.Grid[i, 1].Team = "Black";
                chessBoard.Grid[i, 7].Piece = pieces[i];
                chessBoard.Grid[i, 7].Team = "White";
                chessBoard.Grid[i, 0].Piece = pieces[i];
                chessBoard.Grid[i, 0].Team = "Black";
            }
        }

        // Метод для ініціалізації кнопок та встановлення початкових позицій фігур
        private void populateGrid()
        {
            PopulateButtons();
            SetInitialPiecePositions();
        }

        // Відображає шахові фігури на кнопках відповідно до їхнього розташування
        public void DrawPieces()
        {
            IsKingCheck();
            BoardRenderer.DrawPieces(chessBoard, btnGrid);
        }

        public void ShowLegalMoves(Point location)
        {
            SetBoardColor();

            int _x = location.X;
            int _y = location.Y;

            Cell currentCell = chessBoard.Grid[_x, _y];


            chessBoard.GoThroughAllBoard((x, y) =>
            {
                if (moveCounter > 1)
                {
                    SetBoardColor();
                    return false;
                }

                if (chessBoard.Grid[x, y].IsLegalMove)
                {
                    lastPiece = currentCell.Piece;
                    lastPieceTeam = currentCell.Team;
                }

                if (!chessBoard.Grid[x, y].IsLegalMove)
                    return true;

                btnGrid[x, y].BackColor = (x % 2 == 0 && y % 2 == 0) || (x % 2 != 0 && y % 2 != 0)
                    ? Color.SkyBlue
                    : Color.DeepSkyBlue;

                return true;
            });
        }
        
        public void IsKingCheck()
        {
            chessBoard.legalMovesCounterWhite = 0;
            chessBoard.legalMovesCounterBlack = 0;

            chessBoard.GoThroughAllBoard((x, y) =>
            {
                chessBoard.Grid[x, y].IsCheckPath = false;
                chessBoard.Grid[x, y].IsLegalMove = false;
                chessBoard.Grid[x, y].IsCheckPiece = false;
                chessBoard.Grid[x, y].IsAttackPiece = false;
                chessBoard.Grid[y, x].IsAttackPathWhite = false;
                chessBoard.Grid[y, x].IsAttackPathBlack = false;
                chessBoard.Grid[y, x].LegalMovesCounter = 0;

                chessBoard.isKingCheckWhite = false;
                chessBoard.isKingCheckBlack = false;

                chessBoard.Grid[x, y].IsCurrentlyOccupied = chessBoard.Grid[x, y].Piece != null;

                return true;
            });

            chessBoard.GoThroughAllBoard((x, y) =>
            {
                if (chessBoard.Grid[x, y].Piece == null)
                    return true;

                Cell currentCell = chessBoard.Grid[x, y];
                string chessPiece = chessBoard.Grid[x, y].Piece;
                string pieceTeam = chessBoard.Grid[x, y].Team;

                if (pieceTeam == "White")
                {
                    chessBoard.legalMovesCounter = 0;
                    chessBoard.CheckLegalMoves(currentCell, chessPiece, pieceTeam);
                    chessBoard.legalMovesCounterWhite += chessBoard.legalMovesCounter;
                }

                return true;
            });

            chessBoard.GoThroughAllBoard((x, y) =>
            {
                if (chessBoard.Grid[x, y].Piece == null)
                    return false;

                Cell currentCell = chessBoard.Grid[x, y];
                string chessPiece = chessBoard.Grid[x, y].Piece;
                string pieceTeam = chessBoard.Grid[x, y].Team;


                if (pieceTeam == "Black")
                {
                    chessBoard.legalMovesCounter = 0;
                    chessBoard.CheckLegalMoves(currentCell, chessPiece, pieceTeam);
                    chessBoard.legalMovesCounterBlack += chessBoard.legalMovesCounter;
                }

                return true;
            });

            Console.WriteLine("White legal moves: " + chessBoard.legalMovesCounterWhite);
            Console.WriteLine("Black legal moves: " + chessBoard.legalMovesCounterBlack);
        }

        private void Grid_Button_Click(object sender, EventArgs e)
        {
            this.KeyDown += Game_KeyDown;
            if (!isMoving) return;

            Button clickedButton = (Button)sender;
            Point location = clickedButton.PieceTag().ButtonPosition;
            int _x = location.X;
            int _y = location.Y;

            Cell currentCell = chessBoard.Grid[_x, _y];

            if (IsInvalidMove(currentCell)) return;

            HandleMove(currentCell, location);
        }

        // Перевіряє, чи є хід недійсним
        private bool IsInvalidMove(Cell currentCell)
        {
            if (iswhiteMoving && currentCell.Team != "White" && moveCounter != 1) return true;
            if (!iswhiteMoving && currentCell.Team != "Black" && moveCounter != 1) return true;
            return false;
        }

        // Обробляє хід фігури
        private void HandleMove(Cell currentCell, Point location)
        {
            string pieceTeam = currentCell.Team;
            bool isLegalMove = currentCell.IsLegalMove;

            if (isLegalMove && lastX <= 8 && lastY <= 8)
            {
                chessBoard.Grid[lastX, lastY].Piece = null;
                chessBoard.Grid[lastX, lastY].Team = null;
            }

            lastX = location.X;
            lastY = location.Y;
            moveCounter++;

            chessPiece = currentCell.Piece;

            UpdateBoardOccupancy();

            chessBoard.CheckLegalMoves(currentCell, chessPiece, pieceTeam);
            ShowLegalMoves(location);

            if (isLegalMove && moveCounter == 2)
            {
                CapturePiece(currentCell);
                isMoving = false;
                DrawPieces();
            }

            if (moveCounter > 1)
            {
                ResetMoveState();
                MoveButtonColor();
            }

            PopulateCapturedPieces();

            CheckGameEnd();
        }

        // Оновлює стан зайнятості клітинок на дошці
        private void UpdateBoardOccupancy()
        {
            chessBoard.GoThroughAllBoard((x, y) =>
            {
                chessBoard.Grid[x, y].IsCurrentlyOccupied = chessBoard.Grid[x, y].Piece != null;
                return true;
            });
        }

        // Захоплює фігуру
        private void CapturePiece(Cell currentCell)
        {
            if (chessBoard.Grid[lastX, lastY].Team == "White")
            {
                capturedWhite.Add(chessBoard.Grid[lastX, lastY].Piece);
            }
            else
            {
                capturedBlack.Add(chessBoard.Grid[lastX, lastY].Piece);
            }

            currentCell.Piece = lastPiece;
            currentCell.Team = lastPieceTeam;
        }

        // Скидає стан ходу
        private void ResetMoveState()
        {
            moveCounter = 0;
            lastPiece = null;
            lastX = 30;
            lastY = 30;
        }

        // Перевіряє, чи закінчилася гра
        private void CheckGameEnd()
        {
            if (chessBoard.legalMovesCounterWhite == 0)
            {
                MessageBox.Show("Перемога чорних фігур!");
            }

            if (chessBoard.legalMovesCounterBlack == 0)
            {
                MessageBox.Show("Перемога білих фігур!");
            }
        }

        private void MoveButtonColor()
        {
            buttonPass.BackColor = isMoving ? Color.LimeGreen : Color.Khaki;
        }

        private void buttonPass_Click(object sender, EventArgs e)
        {
            isMoving = !isMoving;
            iswhiteMoving = !iswhiteMoving;
            labelMove.Text = iswhiteMoving ? "Хід білих..." : "Хід чорних...";
            MoveButtonColor();
        }

        private void PopulateCapturedPieces()
        {
            flowLayoutPanelWhite.Controls.Clear();
            flowLayoutPanelBlack.Controls.Clear();

            void SetButtonImageIndex(Button button, string piece)
            {
                button.Height = panel1.Width / (chessBoard.Size * 2);
                button.Width = panel1.Width / (chessBoard.Size * 2);
                button.FlatStyle = FlatStyle.Flat;
                button.FlatAppearance.BorderSize = 0;

                button.ImageIndex = GetImageListIndex(piece);
            }

            if (capturedWhite != null && capturedWhite.Any())
            {
                foreach (var piece in capturedWhite)
                {
                    if (string.IsNullOrEmpty(piece)) continue;

                    Button button = new Button
                    {
                        Tag = piece,
                        ImageList = imageListWhite
                    };
                    SetButtonImageIndex(button, piece);
                    flowLayoutPanelWhite.Controls.Add(button);
                }
            }

            if (capturedBlack != null && capturedBlack.Any())
            {
                foreach (var piece in capturedBlack)
                {
                    if (string.IsNullOrEmpty(piece)) continue;

                    Button button = new Button
                    {
                        Tag = piece,
                        ImageList = imageListBlack
                    };
                    SetButtonImageIndex(button, piece);
                    flowLayoutPanelBlack.Controls.Add(button);
                }
            }
        }

        private int GetImageListIndex(string piece)
        {
            if (string.IsNullOrEmpty(piece))
            {
                throw new ArgumentException("Piece cannot be null or empty");
            }

            string pieceWithoutColor = piece.Replace("White", "").Replace("Black", "");

            switch (pieceWithoutColor)
            {
                case "Pawn":
                    return 0;
                case "Rook":
                    return 1;
                case "Horse":
                    return 2;
                case "Bishop":
                    return 3;
                case "Queen":
                    return 4;
                case "King":
                    return 5;
                default:
                    throw new ArgumentException("Unknown piece: " + piece);
            }
        }

        public void SetBoardColor()
        {
            chessBoard.GoThroughAllBoard((x, y) =>
            {
                btnGrid[x, y].BackColor = ((x % 2 == 0 && y % 2 == 0) || (x % 2 != 0 && y % 2 != 0))
                    ? Color.White
                    : Color.BurlyWood;
                return true;
            });
        }

        private void Game_KeyDown(object sender, KeyEventArgs e)
        {
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Menu menu = new Menu();
            this.Close();
            menu.Show();
        }
    }
}