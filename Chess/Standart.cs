﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using ChessBoard;

namespace Chess
{
    public partial class Standart : Form
    {
        private static Board chessBoard = Board.GetInstance(8);


        public Button[,] btnGrid = new Button[chessBoard.Size, chessBoard.Size];

        public string chessPiece;

        private Timer whiteTimer = new Timer();
        private Timer blackTimer = new Timer();
        private TimeSpan whiteTime = TimeSpan.FromMinutes(5);
        private TimeSpan blackTime = TimeSpan.FromMinutes(5);

        public Standart()
        {
            InitializeComponent();
            InitializeTimers();
            InitializeRestartButton();
            populateGrid();
        }

        private void InitializeTimers()
        {
            whiteTimer.Interval = 1000;
            whiteTimer.Tick += WhiteTimer_Tick;
            whiteTimer.Start();

            blackTimer.Interval = 1000;
            blackTimer.Tick += BlackTimer_Tick;
            blackTimer.Start();
        }

        private void InitializeRestartButton()
        {
            Button buttonRestart = new Button
            {
                Text = "Перезапуск",
                Location = new Point(10, 10),
                Size = new Size(100, 35)
            };
            buttonRestart.Click += buttonRestart_Click;
            Controls.Add(buttonRestart);
        }

        private void buttonRestart_Click(object sender, EventArgs e)
        {
            ResetGame();
        }

        private void ResetGame()
        {
            chessBoard = new Board(8);
            moveCounter = 0;
            lastPiece = null;
            lastX = 30;
            lastY = 30;
            capturedWhite.Clear();
            capturedBlack.Clear();

            panel1.Controls.Clear();
            populateGrid();
        }

        private void WhiteTimer_Tick(object sender, EventArgs e)
        {
            UpdateTimer(ref whiteTime, labelWhiteTime, "Білі", whiteTimer, "Час білих вичерпано!");
        }

        private void BlackTimer_Tick(object sender, EventArgs e)
        {
            UpdateTimer(ref blackTime, labelBlackTime, "Чорні", blackTimer, "Час чорних вичерпано!");
        }

        private void UpdateTimer(ref TimeSpan time, Label label, string team, Timer timer, string timeoutMessage)
        {
            time = time.Subtract(TimeSpan.FromSeconds(1));
            label.Text = $"{team}: {time:mm\\:ss}";

            if (time <= TimeSpan.Zero)
            {
                timer.Stop();
                MessageBox.Show(timeoutMessage);
            }
        }

        private void populateGrid()
        {
            int buttonSize = panel1.Width / chessBoard.Size;
            panel1.Height = panel1.Width;


            chessBoard.GoThroughAllBoard((x, y) =>
            {
                btnGrid[x, y] = new Button();
                btnGrid[x, y].Height = buttonSize;
                btnGrid[x, y].Width = buttonSize;
                btnGrid[x, y].FlatStyle = FlatStyle.Flat;
                btnGrid[x, y].FlatAppearance.BorderSize = 0;


                buttonPass.FlatStyle = FlatStyle.Flat;
                buttonPass.FlatAppearance.BorderSize = 0;
                buttonPass.BackColor = Color.Salmon;


                btnGrid[x, y].Location = new Point(x * buttonSize, y * buttonSize);


                btnGrid[x, y].Tag = new ButtonTag()
                {
                    ButtonPosition = new Point(x, y)
                };

                btnGrid[x, y].Click += Grid_Button_Click;


                panel1.Controls.Add(btnGrid[x, y]);

                return true;
            });

            SetBoardColor();

            string[] pieces = { "Rook", "Horse", "Bishop", "Queen", "King", "Bishop", "Horse", "Rook" };

            for (int i = 0; i < 8; i++)
            {
                chessBoard.Grid[i, 6].Piece = "Pawn";
                chessBoard.Grid[i, 6].Team = "White";
                chessBoard.Grid[i, 1].Piece = "Pawn";
                chessBoard.Grid[i, 1].Team = "Black";
                chessBoard.Grid[i, 7].Piece = pieces[i];
                chessBoard.Grid[i, 7].Team = "White";
                chessBoard.Grid[i, 0].Piece = pieces[i];
                chessBoard.Grid[i, 0].Team = "Black";
            }

            DrawPieces();
        }

        public void DrawPieces()
        {
            IsKingCheck();
            BoardRenderer.DrawPieces(chessBoard, btnGrid);
        }


        private string lastPiece;
        private string lastPieceTeam;
        private int moveCounter = 0;

        public void ShowLegalMoves(Point location)
        {
            SetBoardColor();

            int _x = location.X;
            int _y = location.Y;

            Cell currentCell = chessBoard.Grid[_x, _y];


            chessBoard.GoThroughAllBoard((x, y) =>
            {
                if (moveCounter > 1)
                {
                    SetBoardColor();
                    return false;
                }


                if (chessBoard.Grid[x, y].IsLegalMove)
                {
                    lastPiece = currentCell.Piece;
                    lastPieceTeam = currentCell.Team;
                }

                if (!chessBoard.Grid[x, y].IsLegalMove)
                    return true;

                btnGrid[x, y].BackColor = (x % 2 == 0 && y % 2 == 0) || (x % 2 != 0 && y % 2 != 0)
                    ? Color.SkyBlue
                    : Color.DeepSkyBlue;

                return true;
            });
        }

        public void IsKingCheck()
        {
            chessBoard.legalMovesCounterWhite = 0;
            chessBoard.legalMovesCounterBlack = 0;

            chessBoard.GoThroughAllBoard((x, y) =>
            {
                chessBoard.Grid[x, y].IsCheckPath = false;
                chessBoard.Grid[x, y].IsLegalMove = false;
                chessBoard.Grid[x, y].IsCheckPiece = false;
                chessBoard.Grid[x, y].IsAttackPiece = false;
                chessBoard.Grid[y, x].IsAttackPathWhite = false;
                chessBoard.Grid[y, x].IsAttackPathBlack = false;
                chessBoard.Grid[y, x].LegalMovesCounter = 0;

                chessBoard.isKingCheckWhite = false;
                chessBoard.isKingCheckBlack = false;

                chessBoard.Grid[x, y].IsCurrentlyOccupied = chessBoard.Grid[x, y].Piece != null;

                return true;
            });

            chessBoard.GoThroughAllBoard((x, y) =>
            {
                if (chessBoard.Grid[x, y].Piece == null)
                    return true;

                Cell currentCell = chessBoard.Grid[x, y];
                string chessPiece = chessBoard.Grid[x, y].Piece;
                string pieceTeam = chessBoard.Grid[x, y].Team;

                if (pieceTeam == "White")
                {
                    chessBoard.legalMovesCounter = 0;
                    chessBoard.CheckLegalMoves(currentCell, chessPiece, pieceTeam);
                    chessBoard.legalMovesCounterWhite += chessBoard.legalMovesCounter;
                }

                return true;
            });

            chessBoard.GoThroughAllBoard((x, y) =>
            {
                if (chessBoard.Grid[x, y].Piece == null)
                    return false;

                Cell currentCell = chessBoard.Grid[x, y];
                string chessPiece = chessBoard.Grid[x, y].Piece;
                string pieceTeam = chessBoard.Grid[x, y].Team;

                if (pieceTeam == "Black")
                {
                    chessBoard.legalMovesCounter = 0;
                    chessBoard.CheckLegalMoves(currentCell, chessPiece, pieceTeam);
                    chessBoard.legalMovesCounterBlack += chessBoard.legalMovesCounter;
                }

                return true;
            });

            Console.WriteLine("White legal moves: " + chessBoard.legalMovesCounterWhite);
            Console.WriteLine("Black legal moves: " + chessBoard.legalMovesCounterBlack);
        }

        private int lastX;
        private int lastY;
        private List<string> capturedWhite = new List<string>();
        private List<string> capturedBlack = new List<string>();

        private void Grid_Button_Click(object sender, EventArgs e)
        {
            if (isMoving != true)
            {
                return;
            }


            Button clickedButton = (Button)sender;

            Point location = clickedButton.PieceTag().ButtonPosition;

            int _x = location.X;
            int _y = location.Y;

            Cell currentCell = chessBoard.Grid[_x, _y];


            if (iswhiteMoving && currentCell.Team != "White" && moveCounter != 1)
            {
                return;
            }

            if ((!iswhiteMoving && currentCell.Team != "Black" && moveCounter != 1))
            {
                return;
            }

            string pieceTeam = currentCell.Team;
            bool isLegalMove = currentCell.IsLegalMove;


            if (isLegalMove && lastX <= 8 && lastY <= 8)
            {
                chessBoard.Grid[lastX, lastY].Piece = null;
                chessBoard.Grid[lastX, lastY].Team = null;
            }

            lastX = _x;
            lastY = _y;

            moveCounter++;

            chessPiece = currentCell.Piece;

            chessBoard.GoThroughAllBoard((x, y) =>
            {
                chessBoard.Grid[x, y].IsCurrentlyOccupied = chessBoard.Grid[x, y].Piece != null;
                return true;
            });

            chessBoard.CheckLegalMoves(currentCell, chessPiece, pieceTeam);
            ShowLegalMoves(location);


            if (isLegalMove && moveCounter == 2)
            {
                switch (chessBoard.Grid[lastX, lastY].Team)
                {
                    case "White":
                        capturedWhite.Add(chessBoard.Grid[lastX, lastY].Piece);
                        break;

                    case "Black":
                        capturedBlack.Add(chessBoard.Grid[lastX, lastY].Piece);
                        break;
                }

                currentCell.Piece = lastPiece;
                currentCell.Team = lastPieceTeam;
                isMoving = false;

                DrawPieces();
            }

            if (moveCounter > 1)
            {
                moveCounter = 0;
                lastPiece = null;
                lastX = 30;
                lastY = 30;
                MoveButtonColor();
            }

            populateCapturedPieces();


            if (chessBoard.legalMovesCounterWhite == 0)
            {
                MessageBox.Show("Перемога чорних фігур!");
            }

            if (chessBoard.legalMovesCounterBlack == 0)
            {
                MessageBox.Show("Перемога білих фігур!");
            }
        }

        private void MoveButtonColor()
        {
            if (isMoving)
            {
                buttonPass.BackColor = Color.LimeGreen;
            }
            else
            {
                buttonPass.BackColor = Color.Khaki;
            }
        }

        private bool isFirstMove = true;
        private bool isMoving = false;
        private bool iswhiteMoving = false;
        private int additionalSeconds = 10;

        private void buttonPass_Click(object sender, EventArgs e)
        {
            isMoving ^= true;
            iswhiteMoving ^= true;


            if (iswhiteMoving)
            {
                labelMove.Text = "Хід білих...";
                StartWhiteTimer();
                StopBlackTimer();

                if (!isFirstMove)
                {
                    AddExtraTime(blackTimer);
                }
                else
                {
                    isFirstMove = false;
                }
            }
            else
            {
                labelMove.Text = "Хід чорних...";
                StartBlackTimer();
                StopWhiteTimer();
                AddExtraTime(whiteTimer);
            }

            MoveButtonColor();
        }

        private void AddExtraTime(Timer timer)
        {
            if (timer == whiteTimer)
            {
                whiteTime = whiteTime.Add(TimeSpan.FromSeconds(10));
                labelWhiteTime.Text = "Білі: " + whiteTime.ToString(@"mm\:ss");
            }
            else if (timer == blackTimer)
            {
                blackTime = blackTime.Add(TimeSpan.FromSeconds(10));
                labelBlackTime.Text = "Чорні: " + blackTime.ToString(@"mm\:ss");
            }
        }

        private void StartWhiteTimer()
        {
            whiteTimer.Start();
        }

        private void StopWhiteTimer()
        {
            whiteTimer.Stop();
        }

        private void StartBlackTimer()
        {
            blackTimer.Start();
        }

        private void StopBlackTimer()
        {
            blackTimer.Stop();
        }

        private void populateCapturedPieces()
        {
            flowLayoutPanelWhite.Controls.Clear();
            flowLayoutPanelBlack.Controls.Clear();


            void imgListIndex(Button button)
            {
                button.Height = panel1.Width / (chessBoard.Size * 2);
                button.Width = panel1.Width / (chessBoard.Size * 2);
                button.FlatStyle = FlatStyle.Flat;
                button.FlatAppearance.BorderSize = 0;

                switch (button.Tag)
                {
                    case "Pawn":
                        button.ImageIndex = 0;
                        break;

                    case "Rook":
                        button.ImageIndex = 1;
                        break;

                    case "Horse":
                        button.ImageIndex = 2;
                        break;

                    case "Bishop":
                        button.ImageIndex = 3;
                        break;

                    case "Queen":
                        button.ImageIndex = 4;
                        break;

                    case "King":
                        button.ImageIndex = 5;
                        break;
                }
            }


            if (capturedWhite != null && capturedWhite.Any())
            {
                foreach (var i in capturedWhite)
                {
                    Button button = new Button();
                    button.Tag = i;
                    button.ImageList = imageListWhite;

                    imgListIndex(button);

                    flowLayoutPanelWhite.Controls.Add(button);
                }

                ;
            }

            if (capturedBlack != null && capturedBlack.Any())
            {
                foreach (var i in capturedBlack)
                {
                    Button button = new Button();
                    button.Tag = i;
                    button.ImageList = imageListBlack;

                    imgListIndex(button);

                    flowLayoutPanelBlack.Controls.Add(button);
                }

                ;
            }
        }

        public void SetBoardColor()
        {
            chessBoard.GoThroughAllBoard((x, y) =>
            {
                btnGrid[x, y].BackColor = (x % 2 == 0 && y % 2 == 0) || (x % 2 != 0 && y % 2 != 0)
                    ? Color.White
                    : Color.BurlyWood;
                return true;
            });
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Menu menu = new Menu();
            this.Close();
            menu.Show();
        }
    }
}