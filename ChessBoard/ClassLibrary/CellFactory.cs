﻿namespace ChessBoard.ClassLibrary
{
    public class CellFactory
    {
        public Cell CreateCell(int row, int column)
        {
            return new Cell(row, column);
        }
    }
}
