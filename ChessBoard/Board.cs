﻿using ChessBoard.ClassLibrary;
using System;
using System.Collections.Generic;

namespace ChessBoard
{
    public class Board : IChessObserver
    {
        private List<IChessObserver> observers = new List<IChessObserver>();

        public void AttachObserver(IChessObserver observer)
        {
            observers.Add(observer);
        }

        public void DetachObserver(IChessObserver observer)
        {
            observers.Remove(observer);
        }

        public void NotifyObservers()
        {
            foreach (var observer in observers)
            {
                observer.Update();
            }
        }

        public int Size { get; set; }

        private static Board _instance;
        private static readonly object _lock = new object();
        public Cell[,] Grid { get; set; }

        private CellFactory cellFactory;

        public Board(int size)
        {
            Size = size;
            Grid = new Cell[Size, Size];
            cellFactory = new CellFactory();
            InitializeGrid();
        }

        public static Board GetInstance(int size)
        {
            lock (_lock)
            {
                if (_instance == null)
                {
                    _instance = new Board(size);
                }

                return _instance;
            }
        }

        private void InitializeGrid()
        {
            GoThroughAllBoard((i, j) =>
            {
                Grid[i, j] = cellFactory.CreateCell(i, j);
                return true;
            });
        }

        public void Update()
        {
            Console.WriteLine("Board state has been updated.");
        }

        public bool isKingCheckWhite = false;
        public bool isKingCheckBlack = false;
        public int legalMovesCounterWhite = 0;
        public int legalMovesCounterBlack = 0;
        public int legalMovesCounter = 0;

        private int SwitchOneZeroMinusOne(int switchValue, int i)
        {
            switch (switchValue)
            {
                case 1:
                    return i;
                case 0:
                    return 0;
                case -1:
                    return i * -1;
            }

            return 0;
        }

        public void CheckLegalMoves(Cell currentCell, string chessPiece, string pieceTeam)
        {
            int boardSize = 8;

            GoThroughAllBoard((x, y) =>
            {
                Grid[y, x].IsLegalMove = false;
                return true;
            });

            void CheckPath(int row, int column, int range, bool isCheckPath)
            {
                for (int i = 1; i <= range; i++)
                {
                    int rowNum = SwitchOneZeroMinusOne(row, i);
                    int colNum = SwitchOneZeroMinusOne(column, i);

                    if (currentCell.RowNumber + rowNum < boardSize
                        && currentCell.ColumnNumber + colNum < boardSize
                        && currentCell.RowNumber + rowNum >= 0
                        && currentCell.ColumnNumber + colNum >= 0
                        && Grid[currentCell.RowNumber + rowNum, currentCell.ColumnNumber + colNum].IsLegalMove)
                    {
                        Cell targetCell = Grid[currentCell.RowNumber + rowNum, currentCell.ColumnNumber + colNum];

                        if (!isCheckPath) continue;
                        targetCell.IsCheckPath = true;

                        if (targetCell.IsCurrentlyOccupied)
                        {
                            break;
                        }
                    }
                }
            }

            void CheckAttackPath(int row, int column, int range, bool isKingAttackLineWhite, bool isKingAttackLineBlack)
            {
                for (int i = 1; i <= range; i++)
                {
                    int rowNum = SwitchOneZeroMinusOne(row, i);
                    int colNum = SwitchOneZeroMinusOne(column, i);

                    if (currentCell.RowNumber + rowNum < boardSize
                        && currentCell.ColumnNumber + colNum < boardSize
                        && currentCell.RowNumber + rowNum >= 0
                        && currentCell.ColumnNumber + colNum >= 0
                        && Grid[currentCell.RowNumber + rowNum, currentCell.ColumnNumber + colNum].IsLegalMove)
                    {
                        Cell targetCell = Grid[currentCell.RowNumber + rowNum, currentCell.ColumnNumber + colNum];

                        if (isKingAttackLineWhite)
                        {
                            targetCell.IsAttackPathWhite = true;

                            if (targetCell.Piece == "King")
                            {
                                break;
                            }
                        }

                        if (currentCell.Team == "White"
                            && currentCell.IsAttackPathBlack
                            && !targetCell.IsAttackPathBlack)
                        {
                            targetCell.IsLegalMove = false;
                        }

                        if (isKingAttackLineBlack)
                        {
                            targetCell.IsAttackPathBlack = true;

                            if (targetCell.Piece == "King")
                            {
                                break;
                            }
                        }

                        if (currentCell.Team == "Black"
                            && currentCell.IsAttackPathWhite
                            && !targetCell.IsAttackPathWhite)
                        {
                            targetCell.IsLegalMove = false;
                        }
                    }
                }
            }

            void AttackPath(int row, int column, int range)
            {
                bool isKingAttackLineWhite = false;
                bool isKingAttackLineBlack = false;

                for (int i = 1; i <= range; i++)
                {
                    int rowNum = SwitchOneZeroMinusOne(row, i);
                    int colNum = SwitchOneZeroMinusOne(column, i);

                    if (currentCell.RowNumber + rowNum < boardSize
                        && currentCell.ColumnNumber + colNum < boardSize
                        && currentCell.RowNumber + rowNum >= 0
                        && currentCell.ColumnNumber + colNum >= 0)
                    {
                        Cell targetCell = Grid[currentCell.RowNumber + rowNum, currentCell.ColumnNumber + colNum];

                        if (currentCell.Team == "White"
                            && targetCell.Piece == "King"
                            && targetCell.Team != currentCell.Team)
                        {
                            isKingAttackLineWhite = true;
                            currentCell.IsAttackPiece = true;
                        }

                        if (currentCell.Team == "Black"
                            && targetCell.Piece == "King"
                            && targetCell.Team != currentCell.Team)
                        {
                            isKingAttackLineBlack = true;
                            currentCell.IsAttackPiece = true;
                        }

                        CheckAttackPath(row, column, range, isKingAttackLineWhite, isKingAttackLineBlack);
                    }
                }
            }

            void isValidCell(int row, int column, int range)
            {
                bool isCheckPath = false;

                for (int i = 1; i <= range; i++)
                {
                    int rowNum = SwitchOneZeroMinusOne(row, i);
                    int colNum = SwitchOneZeroMinusOne(column, i);

                    if (currentCell.RowNumber + rowNum < boardSize
                        && currentCell.ColumnNumber + colNum < boardSize
                        && currentCell.RowNumber + rowNum >= 0
                        && currentCell.ColumnNumber + colNum >= 0)
                    {
                        Cell targetCell = Grid[currentCell.RowNumber + rowNum, currentCell.ColumnNumber + colNum];


                        targetCell.IsLegalMove = true;


                        if (currentCell.Team == targetCell.Team)
                        {
                            targetCell.IsLegalMove = false;
                        }


                        switch (currentCell.Team)
                        {
                            case "White":
                            {
                                if (targetCell.Piece == "King"
                                    && targetCell.IsLegalMove
                                    && targetCell.Team != currentCell.Team)
                                {
                                    currentCell.IsCheckPiece = true;
                                    isKingCheckBlack = true;
                                    isCheckPath = true;
                                }


                                if (isKingCheckWhite)
                                {
                                    if (targetCell.IsCheckPath
                                        && targetCell.Team != currentCell.Team)
                                    {
                                        targetCell.IsLegalMove = true;
                                    }

                                    if (!targetCell.IsCheckPath
                                        && targetCell.Team != currentCell.Team)
                                    {
                                        targetCell.IsLegalMove = false;
                                    }

                                    if (targetCell.IsCheckPiece
                                        && targetCell.Team != currentCell.Team)
                                    {
                                        targetCell.IsLegalMove = true;
                                    }
                                }


                                if (currentCell.Piece == "King")
                                {
                                    if (targetCell.IsAttackPathBlack)
                                    {
                                        targetCell.IsLegalMove = false;
                                    }

                                    if (isKingCheckWhite)
                                    {
                                        if (targetCell.IsAttackPathBlack)
                                        {
                                            targetCell.IsLegalMove = false;
                                        }

                                        if (!targetCell.IsAttackPathBlack)
                                        {
                                            targetCell.IsLegalMove = true;
                                        }

                                        if (targetCell.IsCheckPiece)
                                        {
                                            targetCell.IsLegalMove = true;
                                        }
                                    }
                                }

                                break;
                            }
                            case "Black":
                            {
                                if (targetCell.Piece == "King"
                                    && targetCell.IsLegalMove
                                    && targetCell.Team != currentCell.Team)
                                {
                                    currentCell.IsCheckPiece = true;
                                    isKingCheckWhite = true;
                                    isCheckPath = true;
                                }


                                if (isKingCheckBlack)
                                {
                                    if (targetCell.IsCheckPath
                                        && targetCell.Team != currentCell.Team)
                                    {
                                        targetCell.IsLegalMove = true;
                                    }

                                    if (!targetCell.IsCheckPath
                                        && targetCell.Team != currentCell.Team)
                                    {
                                        targetCell.IsLegalMove = false;
                                    }

                                    if (targetCell.IsCheckPiece
                                        && targetCell.Team != currentCell.Team)
                                    {
                                        targetCell.IsLegalMove = true;
                                    }
                                }


                                if (currentCell.Piece == "King")
                                {
                                    if (targetCell.IsAttackPathWhite)
                                    {
                                        targetCell.IsLegalMove = false;
                                    }

                                    if (isKingCheckBlack)
                                    {
                                        if (targetCell.IsAttackPathWhite)
                                        {
                                            targetCell.IsLegalMove = false;
                                        }

                                        if (!targetCell.IsAttackPathWhite)
                                        {
                                            targetCell.IsLegalMove = true;
                                        }

                                        if (targetCell.IsCheckPiece)
                                        {
                                            targetCell.IsLegalMove = true;
                                        }
                                    }
                                }

                                break;
                            }
                        }

                        CheckPath(row, column, range, isCheckPath);

                        if (targetCell.IsLegalMove)
                        {
                            currentCell.LegalMovesCounter++;
                        }

                        if (targetCell.IsCurrentlyOccupied
                            && targetCell.Piece != "King")
                        {
                            goto LoopEnd;
                        }
                    }
                }

                LoopEnd: ;
            }

            void isValidCellPawn(int row, int column, int range)
            {
                for (int i = 1; i <= range; i++)
                {
                    int colNum = SwitchOneZeroMinusOne(column, i);

                    if (currentCell.RowNumber + 0 < boardSize
                        && currentCell.ColumnNumber + colNum < boardSize
                        && currentCell.RowNumber + 0 >= 0
                        && currentCell.ColumnNumber + colNum >= 0)
                    {
                        Grid[currentCell.RowNumber + 0, currentCell.ColumnNumber + colNum].IsLegalMove = true;

                        if (currentCell.Team == Grid[currentCell.RowNumber + 0, currentCell.ColumnNumber + colNum].Team)
                        {
                            Grid[currentCell.RowNumber + 0, currentCell.ColumnNumber + colNum].IsLegalMove = false;
                        }


                        if (isKingCheckBlack
                            && currentCell.Team == "Black")
                        {
                            if (Grid[currentCell.RowNumber + 0, currentCell.ColumnNumber + colNum].IsCheckPath)
                            {
                                Grid[currentCell.RowNumber + 0, currentCell.ColumnNumber + colNum].IsLegalMove = true;
                            }

                            if (!Grid[currentCell.RowNumber + 0, currentCell.ColumnNumber + colNum].IsCheckPath)
                            {
                                Grid[currentCell.RowNumber + 0, currentCell.ColumnNumber + colNum].IsLegalMove = false;
                            }
                        }

                        if (isKingCheckWhite
                            && currentCell.Team == "White")
                        {
                            if (Grid[currentCell.RowNumber + 0, currentCell.ColumnNumber + colNum].IsCheckPath)
                            {
                                Grid[currentCell.RowNumber + 0, currentCell.ColumnNumber + colNum].IsLegalMove = true;
                            }

                            if (!Grid[currentCell.RowNumber + 0, currentCell.ColumnNumber + colNum].IsCheckPath)
                            {
                                Grid[currentCell.RowNumber + 0, currentCell.ColumnNumber + colNum].IsLegalMove = false;
                            }
                        }

                        if (currentCell.Team == "White"
                            && currentCell.IsAttackPathBlack
                            && !Grid[currentCell.RowNumber + 0, currentCell.ColumnNumber + colNum].IsAttackPathBlack)
                        {
                            Grid[currentCell.RowNumber + 0, currentCell.ColumnNumber + colNum].IsLegalMove = false;
                        }

                        if (currentCell.Team == "Black"
                            && currentCell.IsAttackPathWhite
                            && !Grid[currentCell.RowNumber + 0, currentCell.ColumnNumber + colNum].IsAttackPathWhite)
                        {
                            Grid[currentCell.RowNumber + 0, currentCell.ColumnNumber + colNum].IsLegalMove = false;
                        }

                        if (Grid[currentCell.RowNumber + 0, currentCell.ColumnNumber + colNum].IsLegalMove)
                        {
                            currentCell.LegalMovesCounter++;
                        }

                        if (Grid[currentCell.RowNumber + 0, currentCell.ColumnNumber + colNum].IsCurrentlyOccupied)
                        {
                            Grid[currentCell.RowNumber + 0, currentCell.ColumnNumber + colNum].IsLegalMove = false;
                            break;
                        }
                    }
                }
                

                void pawnAttack(int direction)
                {
                    if (currentCell.RowNumber + direction >= 0
                        && currentCell.RowNumber + direction < boardSize)
                    {
                        bool isCheckPath = false;


                        if (Grid[currentCell.RowNumber + row + direction, currentCell.ColumnNumber + column]
                            .IsCurrentlyOccupied)
                        {
                            Grid[currentCell.RowNumber + row + direction, currentCell.ColumnNumber + column]
                                .IsLegalMove = true;


                            if (currentCell.Team == Grid[currentCell.RowNumber + row + direction,
                                    currentCell.ColumnNumber + column].Team)
                            {
                                Grid[currentCell.RowNumber + row + direction, currentCell.ColumnNumber + column]
                                    .IsLegalMove = false;
                            }


                            if (Grid[currentCell.RowNumber + row + direction, currentCell.ColumnNumber + column]
                                    .Piece == "King"
                                && Grid[currentCell.RowNumber + row + direction,
                                    currentCell.ColumnNumber + column].Team != currentCell.Team)
                            {
                                if (Grid[currentCell.RowNumber + row + direction, currentCell.ColumnNumber + column]
                                        .Team == "White")
                                {
                                    isKingCheckWhite = true;
                                }

                                if (Grid[currentCell.RowNumber + row + direction, currentCell.ColumnNumber + column]
                                        .Team == "Black")
                                {
                                    isKingCheckBlack = true;
                                }

                                isCheckPath = true;
                            }

                            if (isCheckPath
                                && Grid[currentCell.RowNumber + row + direction, currentCell.ColumnNumber + column]
                                    .IsLegalMove)
                            {
                                Grid[currentCell.RowNumber + row + direction, currentCell.ColumnNumber + column]
                                    .IsCheckPath = true;
                            }

                            if (isKingCheckWhite
                                && currentCell.Team == "White")
                            {
                                if (Grid[currentCell.RowNumber + row + direction, currentCell.ColumnNumber + column]
                                    .IsCheckPiece)
                                {
                                    Grid[currentCell.RowNumber + row + direction, currentCell.ColumnNumber + column]
                                        .IsLegalMove = true;
                                }
                                else if (!Grid[currentCell.RowNumber + row + direction,
                                             currentCell.ColumnNumber + column].IsCheckPiece)
                                {
                                    Grid[currentCell.RowNumber + row + direction, currentCell.ColumnNumber + column]
                                        .IsLegalMove = false;
                                }
                            }

                            if (isKingCheckBlack
                                && currentCell.Team == "Black")
                            {
                                if (Grid[currentCell.RowNumber + row + direction, currentCell.ColumnNumber + column]
                                    .IsCheckPiece)
                                {
                                    Grid[currentCell.RowNumber + row + direction, currentCell.ColumnNumber + column]
                                        .IsLegalMove = true;
                                }
                                else if (!Grid[currentCell.RowNumber + row + direction,
                                             currentCell.ColumnNumber + column].IsCheckPiece)
                                {
                                    Grid[currentCell.RowNumber + row + direction, currentCell.ColumnNumber + column]
                                        .IsLegalMove = false;
                                }
                            }

                            if (currentCell.Team == "White"
                                && currentCell.IsAttackPathBlack
                                && !Grid[currentCell.RowNumber + row + direction, currentCell.ColumnNumber + column]
                                    .IsAttackPathBlack)
                            {
                                Grid[currentCell.RowNumber + row + direction, currentCell.ColumnNumber + column]
                                    .IsLegalMove = false;

                                if (Grid[currentCell.RowNumber + row + direction, currentCell.ColumnNumber + column]
                                    .IsAttackPiece)
                                {
                                    Grid[currentCell.RowNumber + row + direction, currentCell.ColumnNumber + column]
                                        .IsLegalMove = true;
                                }
                            }

                            if (currentCell.Team == "Black"
                                && currentCell.IsAttackPathWhite
                                && !Grid[currentCell.RowNumber + row + direction, currentCell.ColumnNumber + column]
                                    .IsAttackPathWhite)
                            {
                                Grid[currentCell.RowNumber + row + direction, currentCell.ColumnNumber + column]
                                    .IsLegalMove = false;

                                if (Grid[currentCell.RowNumber + row + direction, currentCell.ColumnNumber + column]
                                    .IsAttackPiece)
                                {
                                    Grid[currentCell.RowNumber + row + direction, currentCell.ColumnNumber + column]
                                        .IsLegalMove = true;
                                }
                            }
                        }

                        if (Grid[currentCell.RowNumber + row + direction, currentCell.ColumnNumber + column]
                            .IsLegalMove)
                        {
                            currentCell.LegalMovesCounter++;
                        }
                    }
                }

                pawnAttack(1);
                pawnAttack(-1);
            }

            bool IsValidMove(int row, int column)
            {
                return row >= 0 && row < boardSize && column >= 0 && column < boardSize;
            }

            void isValidCellHorse(int row, int column)
            {
                bool isCheckPath = false;

                if (currentCell.RowNumber + row < boardSize
                    && currentCell.ColumnNumber + column < boardSize
                    && currentCell.RowNumber + row >= 0
                    && currentCell.ColumnNumber + column >= 0)
                {
                    Cell targetCell = Grid[currentCell.RowNumber + row, currentCell.ColumnNumber + column];

                    targetCell.IsLegalMove = true;

                    if (currentCell.Team == targetCell.Team)
                    {
                        targetCell.IsLegalMove = false;
                    }

                    if (targetCell.Piece == "King"
                        && targetCell.Team != currentCell.Team)
                    {
                        if (Grid[currentCell.RowNumber + row, currentCell.ColumnNumber + column].Team == "White")
                        {
                            isKingCheckWhite = true;
                        }

                        if (Grid[currentCell.RowNumber + row, currentCell.ColumnNumber + column].Team == "Black")
                        {
                            isKingCheckBlack = true;
                        }

                        isCheckPath = true;
                    }


                    if (isCheckPath && targetCell.IsLegalMove)
                    {
                        targetCell.IsCheckPath = true;

                        if (targetCell.IsCurrentlyOccupied)
                        {
                            goto LoopEnd;
                        }
                    }

                    LoopEnd: ;

                    targetCell.IsLegalMove = targetCell.IsCheckPath;

                    if (isKingCheckWhite && currentCell.Team == "White")
                    {
                        targetCell.IsLegalMove = targetCell.IsCheckPath;
                    }

                    if (isKingCheckBlack && currentCell.Team == "Black")
                    {
                        targetCell.IsLegalMove = targetCell.IsCheckPath;
                    }

                    if (targetCell.IsLegalMove)
                    {
                        currentCell.LegalMovesCounter++;
                    }
                }
            }


            switch (chessPiece)
            {
                case "King":
                    try
                    {
                        currentCell.IsCurrentlyOccupied = true;
                    }
                    catch
                    {
                    }

                    isValidCell(0, -1, 1);
                    isValidCell(0, +1, 1);
                    isValidCell(-1, 0, 1);
                    isValidCell(+1, 0, 1);
                    isValidCell(-1, -1, 1);
                    isValidCell(+1, -1, 1);
                    isValidCell(-1, +1, 1);
                    isValidCell(+1, +1, 1);

                    AttackPath(0, -1, 1);
                    AttackPath(0, +1, 1);
                    AttackPath(-1, 0, 1);
                    AttackPath(+1, 0, 1);
                    AttackPath(-1, -1, 1);
                    AttackPath(+1, -1, 1);
                    AttackPath(-1, +1, 1);
                    AttackPath(+1, +1, 1);

                    break;

                case "Queen":
                    try
                    {
                        currentCell.IsCurrentlyOccupied = true;
                    }
                    catch
                    {
                    }

                    isValidCell(0, -1, 7);
                    isValidCell(0, +1, 7);
                    isValidCell(-1, 0, 7);
                    isValidCell(+1, 0, 7);
                    isValidCell(-1, -1, 7);
                    isValidCell(+1, -1, 7);
                    isValidCell(-1, +1, 7);
                    isValidCell(+1, +1, 7);

                    AttackPath(0, -1, 7);
                    AttackPath(0, +1, 7);
                    AttackPath(-1, 0, 7);
                    AttackPath(+1, 0, 7);
                    AttackPath(-1, -1, 7);
                    AttackPath(+1, -1, 7);
                    AttackPath(-1, +1, 7);
                    AttackPath(+1, +1, 7);

                    break;

                case "Rook":
                    try
                    {
                        currentCell.IsCurrentlyOccupied = true;
                    }
                    catch
                    {
                    }

                    isValidCell(0, -1, 7);
                    isValidCell(0, +1, 7);
                    isValidCell(-1, 0, 7);
                    isValidCell(+1, 0, 7);

                    AttackPath(0, -1, 7);
                    AttackPath(0, +1, 7);
                    AttackPath(-1, 0, 7);
                    AttackPath(+1, 0, 7);

                    break;

                case "Bishop":
                    try
                    {
                        currentCell.IsCurrentlyOccupied = true;
                    }
                    catch
                    {
                    }

                    isValidCell(-1, -1, 7);
                    isValidCell(+1, -1, 7);
                    isValidCell(-1, +1, 7);
                    isValidCell(+1, +1, 7);

                    AttackPath(-1, -1, 7);
                    AttackPath(+1, -1, 7);
                    AttackPath(-1, +1, 7);
                    AttackPath(+1, +1, 7);

                    break;

                case "Horse":
                    try
                    {
                        currentCell.IsCurrentlyOccupied = true;
                    }
                    catch
                    {
                    }

                    isValidCellHorse(+2, +1);
                    isValidCellHorse(-2, -1);
                    isValidCellHorse(+2, -1);
                    isValidCellHorse(-2, +1);
                    isValidCellHorse(+1, +2);
                    isValidCellHorse(-1, -2);
                    isValidCellHorse(+1, -2);
                    isValidCellHorse(-1, +2);
                    break;

                case "Pawn":
                    try
                    {
                        currentCell.IsCurrentlyOccupied = true;
                    }
                    catch
                    {
                    }

                    if (pieceTeam == "White")
                    {
                        isValidCellPawn(0, -1, 1);

                        if (currentCell.ColumnNumber == 6)
                        {
                            isValidCellPawn(0, -1, 2);
                        }
                    }

                    if (pieceTeam == "Black")
                    {
                        isValidCellPawn(0, +1, 1);

                        if (currentCell.ColumnNumber == 1)
                        {
                            isValidCellPawn(0, +1, 2);
                        }
                    }

                    break;
            }

            GoThroughAllBoard((x, y) =>
            {
                if (Grid[y, x].IsLegalMove)
                {
                    legalMovesCounter++;
                }

                return true;
            });
        }

        /// <summary>
        ///  Iterates over each cell of a chessboard and performs the specified action.
        /// </summary>
        /// <param name="func">The action to perform on each cell. The action should return <c>true</c> to continue iterating and <c>false</c> to break the loop.</param>
        /// <exception cref="ArgumentNullException">Thrown if <paramref name="action"/> is <c>null</c>.</exception>
        public void GoThroughAllBoard(Func<int, int, bool> func)
        {
            for (int x = 0; x < Size; x++)
            {
                for (int y = 0; y < Size; y++)
                {
                    if (!func.Invoke(x, y))
                        return;
                }
            }
        }
    }
}